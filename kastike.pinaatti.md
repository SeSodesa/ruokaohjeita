# Pinaattikastike

## Esiehdot

1. Pieni kattila
1. 1 dl pakastepinaattia paloina
1. 2 dl ruokakermaa
1. valkopippuria oman maun mukaan
1. 1 rkl vehnäjauhoja.

## Algoritmi

1. Aloitus.
1. Sulata pinaatti mikrossa sulatusteholla.
1. Laita kattila lämpiämään hellalle teholla 7/9.
1. Kaada kerma kattilaan.
1. Lisää vehnäjauho kattilaan.
1. Sekoita tasaiseksi.
1. Laita pinaatit kattilaan.
1. Lisää valkopippuri kattilaan.
1. Kiehauta.
1. Valmis.

## Jälkiehdot

Pinaattikastike.
