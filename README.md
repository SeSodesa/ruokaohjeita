# Ruokaohjeita

Täältä löytyy herkulliseksi todettuja reseptejä. Reseptit mukailevat [Hoaren
kolmikkoja][hoare-triple] ⟨Pre, Alg, Post⟩, missä Pre koostuu algoritmin Alg
esiehdoista ja Post sen jälkiehdoista. Reseptien nimeäminen noudattaa
rakennetta

	lajityyppi.pääraaka-aine.makuprofiili.pääkulttuuri.lisämääreet.md ,

missä lajityyppi ∈ {keitto, pata, kastike, …}, pääraaka-aine ∈ {kana, kala,
tofu, nauta, …}, makuprofiili ∈ {tulinen, suolainen, makea, …}, pääkulttuuri ∈
{suomi, thai, pohjoismaat, välimeri, …} ja  ja lisämääreet yksinkertaisesti
sisältävät mahdollisia huomioita. Viimeisimmät voi jättää pois, jos niille ei
ole tarvetta.

[hoare-triple]: https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple
