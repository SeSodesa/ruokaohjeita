# Tulinen thai-kanakeitto

## Esiehdot

1. Yksi 3 litran kattila
1. 2 rkl ruokaöljyä
1. 300 g maustamattomia kanansuikaleita
1. 400 ml kookosmaitoa (mielellään ei tiivisteestä)
1. 7 tl punaista thai–currytahnaa
1. 3 tl inkivääritahnaa tai vastaava määrä raastettua ikivääriä
1. Chiliä oman maun mukaan, esimerkiksi:
   1. yksi viipaloitu habanero tai
   1. 7 tl *sambal oelek* -tahnaa
1. 1 levy nuudeleita
1. vihreitä pakastepapuja oman maun mukaan
1. pakastepaprikaa oman maun mukaan
1. 1 iso sipuli
1. 500 ml vettä
1. kana- tai kasvisliemikuutio
1. 2 tl soijakastiketta

## Algoritmi

1. Aloitus.
1. Paloittele sipuli.
1. Laita kattila hellalle.
1. Laita kattila lämpiämään teholla 6/9.
1. Lisää öljy kattilaan.
1. Lisää paloitellut sipulit kattilaan.
1. Paista sipulit hädin tuskin läpikuultaviksi.
1. Lisää kana.
1. Lisää thai–currytahna ja sekoita kanan ja sipulien kanssa kunnolla.
1. Paista kunnes kana on kypsää, välillä sekoitellen.
1. Lisää chili.
1. Lisää vesi.
1. Lisää liemikuutio.
1. Lisää kookosmaito.
1. Lisää soijakastike.
1. Sekoita.
1. Kiehauta teholla 7/9.
1. Lisää pavut.
1. Lisää paprikat.
1. Kiehauta.
1. Lisää nuudelit.
1. Keitä kunnes nuudelit kypsyvät.
1. Lisää inkivääri.
1. Sekoita.
1. Valmis.

## Jälkiehdot

Herkullinen thai-tyylinen ja tulinen kanakeitto.
